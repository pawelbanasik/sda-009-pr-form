package com.example.rent.sda_009_pr_form;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.text_view_one)
    protected TextView textViewOne;

    @BindView(R.id.text_view_two)
    protected TextView textViewTwo;

    @BindView(R.id.text_view_three)
    protected TextView textViewThree;

    @BindView(R.id.text_view_four)
    protected TextView textViewFour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("Name")) {
            textViewOne.setText(getIntent().getStringExtra("Name"));
        }

        if (getIntent().hasExtra("Surname")) {
            textViewTwo.setText(getIntent().getStringExtra("Surname"));
        }

        if (getIntent().hasExtra("Age")) {
            textViewThree.setText(getIntent().getStringExtra("Age"));
        }

        if (getIntent().hasExtra("Language")) {
            textViewFour.setText(getIntent().getStringExtra("Language"));
        }

    }

    @OnClick(R.id.button_one)
    public void confirmData(View v) {

        Intent intent = new Intent(getApplicationContext(), Main3Activity.class);
        startActivity(intent);

    }
}
