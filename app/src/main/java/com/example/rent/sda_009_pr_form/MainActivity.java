package com.example.rent.sda_009_pr_form;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_one)
    protected EditText editTextOne;

    @BindView(R.id.edit_text_two)
    protected TextView editTextTwo;

    @BindView(R.id.edit_text_three)
    protected EditText editTextThree;

    @BindView(R.id.spinner_one)
    protected Spinner spinnerOne;

    @BindView(R.id.button_one)
    protected Button buttonOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.languages, android.R.layout.simple_spinner_item);

        // ta linijka nie jest potrzebna (troche zmienia ten wyglad)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOne.setAdapter(adapter);
    }

    @OnClick(R.id.button_one)
    public void clickButtonOne(View v) {

        // utworzenie intentu
        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);

        // dodanie dodatkowego parametru
        intent.putExtra("Name", editTextOne.getText().toString());
        intent.putExtra("Surname", editTextTwo.getText().toString());
        intent.putExtra("Age", editTextThree.getText().toString());
        intent.putExtra("Language", spinnerOne.getSelectedItem().toString());

        startActivity(intent);

    }

}
